import { GravityBalls } from './BouncingBallsModules/gravityBalls.js';
import { StaticBalls } from './BouncingBallsModules/staticBalls.js';
import { BouncingBalls } from './BouncingBallsModules/bouncingBalls.js';
import { generateRandomNumber } from './BouncingBallsModules/UtilityTools.js'

const canvas = document.querySelector('canvas');
canvas.width = window.innerWidth*0.9;
canvas.height = window.innerHeight*0.8; 
const context = canvas.getContext('2d');
const gravityBallsButton = document.getElementById('gravityBalls');
const bouncingBallsButton = document.getElementById('bouncingBalls');
const message = document.getElementById('message');
const staticBallsButton = document.getElementById('staticBalls');
let gravityBalls = new GravityBalls(canvas);
let bouncingBalls = new BouncingBalls(canvas);
let staticBalls = new StaticBalls(canvas);
const colorPallets = [
    ['#155C11', '#35E82C', '#35E82C', '#27A820', '#1E8219'], 
    ['#41E8B0', '#64AC94', '#2C9C76', '#00E69A', '#1E6B51'],
    ['#5C0E06', '#E8240E', '#9C1809', '#A81A0A', '#851408']
];

window.addEventListener('resize', () => {
    canvas.width = window.innerWidth*0.9;
    canvas.height = window.innerHeight*0.8;
})

function cleanCanvas(){
    cancelAnimationFrames();
    clearCanvasForNewAnimation();
    clearCanvasEventListeners();
}

function cancelAnimationFrames(){
    gravityBalls.cancelAnimationFrame();
    bouncingBalls.cancelAnimationFrame();
}

function clearCanvasForNewAnimation(){
    context.clearRect(0, 0, canvas.width, canvas.height);
}

function clearCanvasEventListeners(){
    canvas.removeEventListener('mousedown', gravityBallsCanvasClickListener);
    canvas.removeEventListener('click', staticBallsCanvasClickListener);
}

gravityBallsButton.addEventListener('click', () => {
    cleanCanvas();

    const options = {
        numberOfBalls: 100,
        yMaxVelocity: 30,
        yMinVelocity: 20,
        maxRadius: 50,
        minRadius: 5
    }
    gravityBalls = new GravityBalls(canvas, options);
    gravityBalls.animateBalls();

    canvas.addEventListener('mousedown', gravityBallsCanvasClickListener);
    setActiveButton(gravityBallsButton);
    message.innerText = "Click inside the canvas element to see the balls bounce more and change color!";
})

function gravityBallsCanvasClickListener(){
    gravityBalls.setColorPallet(colorPallets[generateRandomNumber(0, colorPallets.length, true)]);
    gravityBalls.changeBallColors();
    gravityBalls.addVelocityToSystem(10, 100);
    gravityBalls.resetAnimationFrame();
}

function setActiveButton(buttonToSetActive){
    gravityBallsButton.className = "";
    bouncingBallsButton.className = "";
    staticBallsButton.className = "";
    buttonToSetActive.className = "active";
}

bouncingBallsButton.addEventListener('click', () => {
    cleanCanvas();

    const options = {
        numberOfBalls: 100,
        maxRadius: 10,
        minRadius: 20,
        fillColorPallet: ['white'],
        strokeColorPallet: ['#155C11', '#35E82C', '#35E82C', '#27A820', '#1E8219'],
        yMaxVelocity: 2,
        yMinVelocity: 1,
        xMaxVelocity: 2,
        xMinVelocity: 1
    }
    bouncingBalls = new BouncingBalls(canvas, options);
    bouncingBalls.animateBalls();
    setActiveButton(bouncingBallsButton);
    message.innerText = "";
});

staticBallsButton.addEventListener('mousedown', () => cancelAnimationFrames())

staticBallsButton.addEventListener('mouseup', () => {
    cleanCanvas();
    
    const options = {
        numberOfBalls: 150,
        minRadius: 10,
        maxRadius: 50
    }
    staticBalls = new StaticBalls(canvas, options);
    staticBalls.drawBalls();

    canvas.addEventListener('click', staticBallsCanvasClickListener);
    setActiveButton(staticBallsButton);
    message.innerText = "Click inside the canvas element to see the balls change color!"
})

function staticBallsCanvasClickListener(){
    staticBalls.setColorPallet(colorPallets[generateRandomNumber(0, colorPallets.length, true)]);
    staticBalls.changeBallColors();
}


