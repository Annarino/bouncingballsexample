export class Ball{
    constructor(newBall = {}){
        this.color = newBall.color ;
        this.strokeColor = newBall.strokeColor || undefined;
        this.xCoordinate = newBall.xCoordinate || undefined;
        this.yCoordinate = newBall.yCoordinate || undefined;
        this.mass = newBall.mass || undefined;
        this.radius = newBall.radius || undefined;
        this.startingVelocity = newBall.startingVelocity || undefined;
        this.yVelocity = newBall.yVelocity || undefined;
        this.xVelocity = newBall.xVelocity || undefined;
        this.velocityLossToFriction = newBall.velocityLossToFriction || undefined;
        this.frictionCoefficient = newBall.frictionCoefficient || undefined;
        this.minimumVelocity = newBall.minimumVelocity || undefined;
        this.timeInterval = newBall.timeInterval || undefined;
    }

    setColor(color){
        this.color = color;
    }

    setStrokeColor(color){
        this.strokeColor = color;
    }

    setXCoordinate(xCoordinate){
        this.xCoordinate = xCoordinate;
    }

    setYCoordinate(yCoordinate){
        this.yCoordinate = yCoordinate;
    }

    setMass(mass){
        this.mass = mass;
    }

    setRadius(radius){
        this.radius = radius;
    }

    setStartVelocity(velocity){
        this.startingVelocity = velocity;
    }

    setYVelocity(velocity){
        this.yVelocity = velocity;
    }

    setXVelocity(velocity){
        this.xVelocity = velocity;
    }

    setVelocityLossToFriction(velocityAfterFrictionIsApplied){
        this.velocityLossToFriction = velocityAfterFrictionIsApplied;
    }

    setFrictionCoefficient(friction){
        this.frictionCoefficient = friction;
    }

    setMinimumVelocity(velocity){
        this.minimumVelocity = velocity;
    }

    setTimeInterval(intervalInSeconds){
        this.timeInterval = intervalInSeconds;
    }

    getColor(){
        return this.color;
    }

    getStrokeColor(){
        return this.strokeColor;
    }

    getXCoordinate(){
        return this.xCoordinate;
    }

    getYCoordinate(){
        return this.yCoordinate;
    }

    getMass(){
        return is.mass;
    }

    getRadius(){
        return this.radius;
    }

    getStartVelocity(){
        return this.startingVelocity;
    }

    getYVelocity(){
        return this.yVelocity;
    }

    getXVelocity(){
        return this.xVelocity;
    }

    getVelocityLossToFriction(){
        return this.velocityLossToFriction;
    }

    getFrictionCoefficient(){
        return this.frictionCoefficient;
    }

    getMinimumVelocity(){
        return this.minimumVelocity;
    }

    getTimeInterval(){
        return this.timeInterval;
    }
}