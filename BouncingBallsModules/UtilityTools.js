export function generateRandomNumber(min = 0, max = 1, roundDown = false){
    if(roundDown){
        return Math.floor(Math.random() * (max - min) + min);
    }
 return Math.random() * (max - min) + min;
}

export function generateRandomSign(){
    const sign = Math.random() < 0.5 ? -1 : 1;
    return sign;
}