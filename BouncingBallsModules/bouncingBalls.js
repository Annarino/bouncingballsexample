import {generateRandomNumber, generateRandomSign} from './UtilityTools.js';
import {Ball} from './Ball.js';

export class BouncingBalls{
    constructor(canvas, options = {}){
        this.canvas = canvas;
        this.context = canvas.getContext('2d');
        this.animate = true;
        this.balls = new Array(options.numberOfBalls || 1);
        this.maxRadius = options.maxRadius || 100;
        this.minRadius = options.minRadius || 1;
        this.fillColorPallet = options.fillColorPallet || ['rgba(0, 0, 255, 0.3)'];
        this.fillBall = options.fillCircle || true;
        this.strokeColorPallet = options.strokeColorPallet || [ '#1f1f1f'];
        this.xMaxVelocity = options.xMaxVelocity || 10;
        this.xMinVelocity = options.xMinVelocity || 5;
        this.yMaxVelocity = options.yMaxVelocity || 10;
        this.yMinVelocity = options.yMinVelocity || 5;

        this.instantiateBalls();
    }

    instantiateBalls(){
        for(let i = 0; i < this.balls.length; i++){
            const ball = new Ball;
            let radius = generateRandomNumber(this.minRadius, this.maxRadius, true);
            console.log(radius)
                ball.setRadius(radius);
                ball.setXCoordinate(generateRandomNumber(this.canvas.width - ball.radius, ball.radius, true));
                ball.setYCoordinate(generateRandomNumber(this.canvas.height - ball.radius, ball.radius, true));
                ball.setXVelocity(generateRandomNumber(this.xMinVelocity, this.xMaxVelocity) * generateRandomSign());
                ball.setYVelocity(generateRandomNumber(this.yMinVelocity, this.yMaxVelocity) * generateRandomSign());
                ball.setColor(this.generateRandomColor(this.fillColorPallet));
                ball.setStrokeColor(this.generateRandomColor(this.strokeColorPallet));

            this.balls[i] = ball;
        }
    }

    animateBalls(){
        let animationFrame;
        if(this.animate){
             animationFrame = requestAnimationFrame(() => this.animateBalls());
        } else {
            cancelAnimationFrame(animationFrame);
            this.context.clearRect(0,0,this.canvas.width, this.canvas.height)
        }

        this.context.clearRect(0,0, this.canvas.width, this.canvas.height);
        for(let ball of this.balls){
            this.drawBall(ball)
            this.updateXPosition(ball);
            this.updateYPosition(ball)
        }
    }

    drawBall(ball){
        this.context.beginPath();
        this.context.arc(ball.getXCoordinate(), ball.getYCoordinate(), ball.getRadius(), 0, 2*Math.PI, false);
        this.context.fill()
        this.context.stroke();
        this.context.strokeStyle = ball.getStrokeColor();
        this.context.fillStyle = ball.getColor();
    }

    updateXPosition(ball){
        let updatedXPosition;
        
        if(ball.getXCoordinate() + ball.getRadius() >= this.canvas.width || ball.getXCoordinate() - ball.getRadius() <= 0){
            ball.setXVelocity(-ball.getXVelocity());
        } 
        updatedXPosition = ball.getXCoordinate() + ball.getXVelocity();

        ball.setXCoordinate(updatedXPosition);
    }

    updateYPosition(ball){
        let updatedYPosition;

        if(ball.getYCoordinate() + ball.getRadius() >= this.canvas.height || ball.getYCoordinate() - ball.getRadius() <= 0){
            ball.setYVelocity(-ball.getYVelocity());
        }
        updatedYPosition = ball.getYCoordinate() + ball.getYVelocity();

        ball.setYCoordinate(updatedYPosition);
    }

    generateRandomColor(pallet){
        let color;

        if(pallet.length === 1){
            color = pallet[0];
        } else {
            color = pallet[generateRandomNumber(0, pallet.length, true)];
        }

        return color
    }

    cancelAnimationFrame(){
        this.animate = false;
    }
}



